﻿namespace Benchmarks.HelperObjects.Inheritance; 

public abstract class ADoable {
	public abstract int UpdateAndGetValue();
}