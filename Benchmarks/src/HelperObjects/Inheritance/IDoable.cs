﻿namespace Benchmarks.HelperObjects.Inheritance; 

public interface IDoable {

	int UpdateAndGetValue();
}